package main

import (
	"flag"
	"fmt"
	"github.com/stianeikeland/go-rpio"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var (
	address string
	version bool
	verbose bool
	logLoc  string
)

func sameline(printthis string) {
	fmt.Printf("\033[s%s\033[u", printthis)
}

func slowly(inputtext string, delay int) {
	passes := len(inputtext)
	for i := 0; i <= passes; i++ {
		fmt.Printf("%s", string(inputtext[i]))
		time.Sleep(time.Duration(delay) * time.Millisecond)
		if i == (passes - 1) {
			break
		}
	}
}

func sigCatch() {
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM, syscall.SIGKILL)
	go func() {
		<-c
		for x := 0; x < 100; x++ {
			text := fmt.Sprintf(" ): terminating %s %d :( ", os.Args[0], x)
			time.Sleep(time.Millisecond * time.Duration(15))
			sameline(text)
		}
		fmt.Printf(" ): TERMINATED %s :(     \n", os.Args[0])
		os.Exit(1)
	}()
}

func site_check(addressToCheck string) (bool, string, error) {
	data, err := http.Get(addressToCheck)
	if err != nil {
		if verbose {
			fmt.Printf("err: %s", err)
		}
		return false, "false", err
	} else {
		return true, data.Status, err
	}
}

func currentTime() string {
	now := time.Now()
	return fmt.Sprintf("%s,%d,%d,%d,%d",now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second())
}

func internetCheck() bool {
	var siteCount = 0
	timeNow := currentTime()
	googleBool, googleCode, _ := site_check("https://google.com/")
	piaBool, piaCode, _ := site_check("https://www.privateinternetaccess.com/")
	bingBool, bingCode, _ := site_check("https://bing.com/")
	if googleBool && googleCode == "200 OK" {
		if verbose {
			fmt.Println("Google is up count++")
		}
		siteCount++
	}
	if piaBool && piaCode == "200 OK" {
		if verbose {
			fmt.Println("Private Internet Access is up count++")
		}
		siteCount++
	}
	if bingBool && bingCode == "200 OK" {
		if verbose {
			fmt.Println("Bing is up count++")
		}
		siteCount++
	}
    if siteCount >= 1 {
		if verbose {
			fmt.Println("internet is up " + timeNow)
		}
		return true
	} else {
		if verbose {
			fmt.Println("internet is down " + timeNow)
		}
		return false
	}
}

func siteCheck() bool {
	timeNow := currentTime()
	if len(address) > 5 {
		siteBool, statusCode, _ := site_check(address)
		if siteBool && statusCode == "200 OK" {
			if verbose {
				fmt.Println(address + " is up " + timeNow)
			}
			return true
		} else {
			if verbose {
				fmt.Println(address + " is down " + timeNow)
			}
			return false
		}
	}
	return false
}

func logDown(address string) {
	file, _ := os.OpenFile(logLoc, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666) // all hail satan, LOL
	writeThis := fmt.Sprintf("%s, %s\n", address, currentTime())
	file.WriteString(writeThis)
	file.Close()
}

func resetModem(relayPin int) error {
	if verbose {
		fmt.Printf("modem reset\n")
	}
	err := rpio.Open()
	if err != nil {
		return err
	}
	relay := rpio.Pin(relayPin)
	relay.Output()
	if verbose {
		fmt.Println("modem off")
	}
	relay.High()
	time.Sleep(time.Second * time.Duration(3)) // let power drain
	if verbose {
		fmt.Println("modem on")
	}
	relay.Low()
	time.Sleep(time.Second * time.Duration(60)) // give modem time to reconnect
	return nil
}

func main() {
	go sigCatch()
	var gpioPin int
	var license bool
	flag.BoolVar(&license, "license", false, "show license and exit")
	flag.IntVar(&gpioPin, "gpio", 0, "relay gpio to reset modem")
	flag.StringVar(&address, "address", "", "website address to check if up\nFormat {https://bitbucket.org}")
	flag.BoolVar(&verbose, "verbose", false, "be verbose")
	flag.BoolVar(&version, "version", false, "version")
	flag.StringVar(&logLoc, "log", "", "log file")
	flag.Parse()
	if license {
		fmt.Println(`   is-up  Copyright (C) 2018  scott snyder

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.`)
		os.Exit(0)
	}
	if version {
		slowly("is-up version -0.2a\n https://bitbucket.org/ul7r41337h4x0r/is-up\n", 42)
		os.Exit(0)
	}
	if internetCheck() {
		if len(address) > 5 {
			if !siteCheck() {
				if len(logLoc) > 1 {
					logDown(address)
				}
				os.Exit(3)
			} else {
				os.Exit(0)
			}
		} else {
			os.Exit(0)
		}
		os.Exit(0)
	} else {
		if len(logLoc) > 1 {
			if gpioPin != 0 {
				resetModem(gpioPin)
			}
			logDown("internet")
		}
		os.Exit(3)
	}
}
