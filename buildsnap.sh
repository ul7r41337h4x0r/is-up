#!/bin/bash

#Ubuntu 18.04 does not support   "--target-arch='i386'" ?
for x0 in "--target-arch=amd64" "--target-arch=armhf" "--target-arch=arm64"; do
    echo  "snapcraft ${x0}"
    snapcraft ${x0}
    snapcraft clean is-up -s pull
done


for x1 in *.snap; do
    snapcraft push ${x1}
done
